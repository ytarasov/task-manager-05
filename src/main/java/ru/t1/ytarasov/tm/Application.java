package ru.t1.ytarasov.tm;
import static ru.t1.ytarasov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            displayError();
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case ABOUT:
                displayAbout();
                break;
            case VERSION:
                displayVersion();
                break;
            case HELP:
                displayHelp();
                break;
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Display program version.\n", VERSION);
        System.out.printf("%s - Display developer info.\n", ABOUT);
        System.out.printf("%s - Display list of terminal commands.\n", HELP);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.1");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuriy Tarasov");
        System.out.println("ytarasov@t1-consulting.ru");
    }

    private  static void displayError() {
        System.out.println("Error. Type help to show availible commands");
    }

}
